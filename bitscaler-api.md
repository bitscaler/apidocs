HOST:  https://portal.bitscaler.com

# BitScaler API Documentation

# Group Node API
API methods to manipulate nodes.  Nodes are different from other entities in the BitScaler system in that while there are some basic attributes you can add any attribute to a node you want.  You are not constrained to the list below.  You are limited to 40 unique fields and no field length can be greater than 128k.

+ Node Attributes
    + node (string required) the name of the node.  must be unique
    + type (string required) server|switch|router|lb
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2
    + agents (array on get/list optional) A list of stat agents being run on this node
    + blockdevices (array of strings optional) A list of block devices for this node if applicable
    + updated (integer read-only) Epoch time of when document was last updated
    + interfaces (array of strings optional) A list of interfaces for this node
    + mountpoints (array of strings optional) A list of filesystem mount points for the node if applicable
    + <generic>  Users can store any key/value properties they choose that are not listed above.  Max of 40 fields / 128k per field


## node methods [/api/2.0/node/{id}]

### create node [POST]
create a node

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>
          Content-Type: application/json

    + Body

          {
                "os": "Ubuntu 14.04 LTS",
                "cpus": 12,
                "node": "runscope.fll1.bitscaler.net",
                "cpuSpeed": "3.33GHz",
                "agents": [
                    {
                        "modules": [
                            "cpu",
                            "disk-io",
                            "interfaces",
                            "load",
                            "netstat",
                            "memory-stats",
                            "disk-usage",
                            "vmstat",
                            "netstat-protocols",
                            "versions",
                            "ntp",
                            "process-stats"
                        ],
                        "agent": "system_stats",
                        "version": "4.10"
                    }
                ],
                "kernel": "3.13.0-30-generic",
                "type": "server",
                "memory": "24GB"
          }

+ Response 200

      + Headers

          Content-Type: application/json

      + Body

          {
                  "message": "Ok",
                  "status": 200,
                  "totalRecords": 1,
                  "records": [
                      {
                          "id": "e60d7a94-57b9-4209-81d3-827d8f9702bd",
                          "os": "Ubuntu 14.04 LTS",
                          "cpus": 12,
                          "node": "runscope.fll1.bitscaler.net",
                          "cpuSpeed": "3.33GHz",
                          "updated": 1406410202,
                          "agents": [
                              {
                                  "updated": 1406410202,
                                  "modules": [
                                      "cpu",
                                      "disk-io",
                                      "interfaces",
                                      "load",
                                      "netstat",
                                      "memory-stats",
                                      "disk-usage",
                                      "vmstat",
                                      "netstat-protocols",
                                      "versions",
                                      "ntp",
                                      "process-stats"
                                  ],
                                  "agent": "system_stats",
                                  "version": "4.10"
                              }
                          ],
                          "kernel": "3.13.0-30-generic",
                          "type": "server",
                          "memory": "24GB"
                      }
                  ]
          }


### get node [GET]
Retrieve a node given an ID or node name.  To retrieve a list simply omit the ID.

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

          Content-Type: application/json

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "e60d7a94-57b9-4209-81d3-827d8f9702bd",
                      "os": "Ubuntu 14.04 LTS",
                      "cpus": 12,
                      "node": "runscope.fll1.bitscaler.net",
                      "cpuSpeed": "3.33GHz",
                      "updated": 1406410202,
                      "agents": [
                          {
                              "updated": 1406410202,
                              "modules": [
                                  "cpu",
                                  "disk-io",
                                  "interfaces",
                                  "load",
                                  "netstat",
                                  "memory-stats",
                                  "disk-usage",
                                  "vmstat",
                                  "netstat-protocols",
                                  "versions",
                                  "ntp",
                                  "process-stats"
                              ],
                              "agent": "system_stats",
                              "version": "4.10"
                          }
                      ],
                      "kernel": "3.13.0-30-generic",
                      "type": "server",
                      "memory": "24GB"
                  }
              ]
           }

### update node [PUT]
Update a node.  You only need to send the attributes you wish to modify.

+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>

    + Body

          {
              "cpus": 24
          }

+ Response 200

    + Headers

          Content-Type: application/json

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "e60d7a94-57b9-4209-81d3-827d8f9702bd",
                      "os": "Ubuntu 14.04 LTS",
                      "node": "runscope.fll1.bitscaler.net",
                      "cpuSpeed": "3.33GHz",
                      "cpus": 24,
                      "updated": 1406410207,
                      "agents": [
                          {
                              "updated": 1406410202,
                              "modules": [
                                  "cpu",
                                  "disk-io",
                                  "interfaces",
                                  "load",
                                  "netstat",
                                  "memory-stats",
                                  "disk-usage",
                                  "vmstat",
                                  "netstat-protocols",
                                  "versions",
                                  "ntp",
                                  "process-stats"
                              ],
                              "agent": "system_stats",
                              "version": "4.10"
                          }
                      ],
                      "type": "server",
                      "kernel": "3.13.0-30-generic",
                      "memory": "24GB"
                  }
              ]
           }

### Delete node [DELETE]
Delete a node

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

           Content-Type: application/json

   + Body

           {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
           }


# Group Metric Object API
API methods to search and manipulate metric objects.  You should not need to create your own metrics.  This happens automatically upon delivery of new metrics via a stat agent.

+ Metric Attributes
    + id (string) This is the full metric name. IE md02.iad1.bitscaler.net#cpu#cpuTotal#busyPercentage
    + description (string) A textual string describing what the metric is.  This is what appears in the graph builder
    + node (string read-only) This is the node name that statistic applies to. Eg db01.sjc1.company.net
    + stat (string read-only) This is the stat name. Eg cpuBUsy
    + category (string read-only)  The category is the top level name directly to the right of the node box in the graph builder.
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + interval (integer optional) Update interval for this metric.  This is set automatically by the BitScaler metric system
    + lastUpdated (integer auto set) This is auto set.  It is the epoch timestamp of the metric
    + noVirtual (boolean optoinal only on list) Don't return virtual metrics
    + tags (array optional) Array of tags for this metric.  Some are system auto generated and some are user generated
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2

## metric crud methods [/api/2.0/metric/{id}]

### create metric [POST]
Create a new metric.  This happens automatically when you submit new metrics via any stat agent so this shouldn't be used under normal circumstances.

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "company": "bitscaler",
            "node": "node1.test.bitscaler.net",
            "category": "mycategory",
            "stat": "mystat"
        }

+ Response 200

    + Headers

        Content-Type: application/json
        X-Response-Time: 4ms

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "node1.test.bitscaler.net#mycategorye#mystat",
                    "tags": [
                        {
                            "system": true,
                            "values": [
                                "node1.test.bitscaler.net"
                            ],
                            "type": "node"
                        },
                        {
                            "system": true,
                            "values": [
                                "mycategory"
                            ],
                            "type": "agent"
                        },
                        {
                            "system": true,
                            "values": [
                                "System"
                            ],
                            "type": "owner"
                        },
                        {
                            "system": true,
                            "values": [
                                "metric"
                            ],
                            "type": "entity"
                        },
                        {
                            "system": true,
                            "values": [
                                "org"
                            ],
                            "type": "visibility"
                        }
                    ],
                    "node": "node1.test.bitscaler.net",
                    "category": "mycategory",
                    "stat": "mystat"
                }
            ]
        }

### get metric [GET]
Return a metric object given its ID or return a list of metric if no ID is provided

+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                        {
                            "node": "ds01.fll1.bitscaler.net",
                            "category": "company_stats",
                            "count": 1,
                            "stat": "masterCompany:subCompanyMetricsSecond"
                        }
                      ],
                      "columns": 1,
                      "filename": "test-dashboard-filename",
                      "updateInterval": 60
                  }
              ]
          }

## update metric [PUT]
Update the various attributes of a metric

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "description": "This statistic measures how fast the earth spins"
        }

+ Response 200

    + Headers

        Content-Type: application/json
        X-Response-Time: 4ms

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "node1.test.bitscaler.net#mycategorye#mystat",
                    "tags": [
                        {
                            "system": true,
                            "values": [
                                "node1.test.bitscaler.net"
                            ],
                            "type": "node"
                        },
                        {
                            "system": true,
                            "values": [
                                "mycategory"
                            ],
                            "type": "agent"
                        },
                        {
                            "system": true,
                            "values": [
                                "System"
                            ],
                            "type": "owner"
                        },
                        {
                            "system": true,
                            "values": [
                                "metric"
                            ],
                            "type": "entity"
                        },
                        {
                            "system": true,
                            "values": [
                                "org"
                            ],
                            "type": "visibility"
                        }
                    ],
                    "node": "node1.test.bitscaler.net",
                    "category": "mycategory",
                    "stat": "mystat",
                    "description": "This statistic measures how fast the earth spins"
                }
            ]
        }

### Delete metric [DELETE]
Delete a metric.

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }


## metric count methods [/api/2.0/metric/counts]

### get metric counts [GET]
Use this method to retrieve metrics counts by any combination of node, category and/or stat

+ Attributes
    + node (string) name of the node to retrieve metrics for
    + level (string) what level to return. levels are represented by a '#'.  Possible values are category and stat. Defaults to returning all levels.

+ Request node=test1.fll1.bitscaler.net

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 4,
              "records": [
                  {
                      "node": "node1.test.bitscaler.net",
                      "count": 321
                  },
                   {
                      "node": "node2.test.bitscaler.net",
                      "count": 899
                  },
                   {
                      "node": "node3.test.bitscaler.net",
                      "count": 55
                  },
                   {
                      "node": "node4.test.bitscaler.net",
                      "count": 56
                  }  
              ]
          }

## node list methods [/api/2.0/metric/listNodes]

### get nodes [GET]
Use this method to retrieve all the node names

+ Attributes
    + This call currently has no attributes and simply returns a list of nodes in the system

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 4,
              "records": [
                  {
                      "node": "node1.test.bitscaler.net"
                  },
                   {
                      "node": "node2.test.bitscaler.net"
                  },
                   {
                      "node": "node3.test.bitscaler.net"
                  },
                   {
                      "node": "node4.test.bitscaler.net"
                  }  
              ]
          }

## metric bynode methods [/api/2.0/metric/bynode]

### get metrics by node [GET]
Use this method to retrieve all the metrics for a node or set of nodes

+ Attributes
    + node (string) the node you would like metrics for.  For multiple nodes simpley do node=aaa&node=bbb
    + outputFormat (string) the field(s) you would like returned. Possible values id, category, node, stat, description,

+ Request ?node=test01.fll1.bitscaler.net&outputField=stat&outputField=category

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 3,
              "records": [
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat",
                      "node": "ds01.fll1.bitscaler.net",
                      "category": "my_stats",
                      "stat": "mystat"
                  },
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat1",
                      "node": "ds01.fll1.bitscaler.net",
                      "category": "my_stats",
                      "stat": "mystat1"
                  },
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat2",
                      "node": "ds01.fll1.bitscaler.net",
                      "category": "my_stats",
                      "stat": "mystat2"
                  }
              ]
          }

## metric regex methods [/api/2.0/metric/re]

### get metrics by regex [GET]
Use this method to retrieve all the metrics given a regular expression.  This will return a list of metric ids

+ Attributes
    + regex (string) the regular expression to match with.  

+ Request ?regex=stat\d*$

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 3,
              "records": [
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat"
                  },
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat1"
                  },
                  {
                      "id": "ds01.fll1.bitscaler.net#my_stats#mystat2"
                  }
              ]
          }


# Group Chart Object API
API methods to manipulate chart objects

+ Chart Attributes
    + id (uuid)  IDs are autogenerated and will generate an error if sent on creation.  It is returned upon successful creation.
    + filename (string) Name of the chart in saved items.
    + startTime (epoch or negative values for relative time frames.  IE -3600 for the last hour etc)
    + endTime (epoch or 0 for relative time frames)
    + height (optional) chart height in pixels. Default 600
    + width (optional) chart width in pixels.  Default 800
    + ninetyFifths (optional boolean) specifies whether or not to calculate 95th percentile on series. Default false
    + title (optional) Title that will be seen on the chart
    + tags (optional array) Tags that can be used for filtering and locating the chart
    + series (array of series objects) A series a an object that describes what metrics, interval, color, label, horizontal rule and complex equations
    + outputFormat (array of strings) Specify what fields to return as part of the object.  Default is all fields.
    + autosave (optional boolean default false) Set to true if you would like this chart to expire if not accessed ( currently 36 hours )
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + dashboard (optional boolean default false) Use the dashboard style on this instead of the chart style
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2
    + terseLegend (optional boolean only for chart retrieval) De-duplicate legend data such as hostnames, stat names etc
    + timeZone (optional String) Set the timezone for this chart
    + xtitle (optional string) x axis label
    + ytitle (optional string) y axis label

+ series
    + type (string) metric | equation | hrule
    + db (string) RAW | MIN | MAX | AVG | SUM
    + interval (integer) Number of seconds to roll the data up to.  IE 86400 for daily
    + color (hex string) The color for the series
    + metrics (array) An array of strings that represents all the metrics to be used in the series.  IE md02.iad1.bitscaler.net#cpu#cpuTotal#busyPercentage
    + equation (string) Equation to be used across the metrics.  Use A, B, C etc for representing metrics.  IE. A+B+C or simply SUM to summ all
    + label (string optional) What will appear in the graph legend
    + value (numerical) Value at which to draw a horizontal rule.  This is only used if series type is hrule.


## chart methods [/api/2.0/chart/{id}]

### create chart [POST]
Create a chart.  ID is invalid on creation.  The UUID will be returned as the result of a sucessful creation.


+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>

    + Body

          {
                "startTime": -21600,
                "endTime": 0,
                "filename": "runscope-chart",
                "series": [
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu0#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu1#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu2#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu3#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu4#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu5#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu6#busyPercentage"
                        ]
                    },
                    {
                        "type": "metric",
                        "db": "RAW",
                        "interval": 0,
                        "metrics": [
                            "ds01.fll1.bitscaler.net#cpu#cpu7#busyPercentage"
                        ]
                    }
                ]
            }

+ Response 200
    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "e36c6db9-4d53-4dd8-8ada-81dbf9be1879",
                      "tags": [
                          {
                              "system": true,
                              "values": [
                                  "ds01.fll1.bitscaler.net"
                              ],
                              "type": "node"
                          },
                          {
                              "system": true,
                              "values": [
                                  "cpu"
                              ],
                              "type": "agent"
                          },
                          {
                              "system": true,
                              "values": [
                                  "bitscaler-API"
                              ],
                              "type": "owner"
                          },
                          {
                              "system": true,
                              "values": [
                                  "chart"
                              ],
                              "type": "entity"
                          },
                          {
                              "system": true,
                              "values": [
                                  "org"
                              ],
                              "type": "visibility"
                          },
                          {
                              "system": true,
                              "values": [
                                  "busyPercentage",
                                  "cpu0",
                                  "cpu1",
                                  "cpu2",
                                  "cpu3",
                                  "cpu4",
                                  "cpu5",
                                  "cpu6",
                                  "cpu7"
                              ],
                              "type": "metric"
                          }
                      ],
                      "startTime": -21600,
                      "series": [
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu0#busyPercentage"
                              ],
                              "color": "FF0000",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu1#busyPercentage"
                              ],
                              "color": "0000FF",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu2#busyPercentage"
                              ],
                              "color": "00FF00",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu3#busyPercentage"
                              ],
                              "color": "F7FF00",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu4#busyPercentage"
                              ],
                              "color": "00FFFF",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu5#busyPercentage"
                              ],
                              "color": "FF00FF",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu6#busyPercentage"
                              ],
                              "color": "FF8000",
                              "type": "metric"
                          },
                          {
                              "db": "RAW",
                              "interval": 0,
                              "metrics": [
                                  "ds01.fll1.bitscaler.net#cpu#cpu7#busyPercentage"
                              ],
                              "color": "7500CE",
                              "type": "metric"
                          }
                      ],
                      "filename": "runscope-chart",
                      "ninetyFifths": false,
                      "type": "chart",
                      "endTime": 0
                  }
              ]
          }



### retrieve charts [GET]
Return a chart document by id.  You can retrieve a list of charts by omitting the ID.

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
            "message": "Ok",
            "status": 200,
            "totalRecords": 2,
            "records": [
                          {
                              "tags": [
                                  {
                                      "system": true,
                                      "values": [
                                          "md01.iad1.bitscaler.net",
                                          "md02.iad1.bitscaler.net"
                                      ],
                                      "type": "node"
                                  },
                                  {
                                      "system": true,
                                      "values": [
                                          "cpu"
                                      ],
                                      "type": "agent"
                                  },
                                  {
                                      "system": true,
                                      "values": [
                                          "dmurphy@bitscaler.com"
                                      ],
                                      "type": "owner"
                                  },
                                  {
                                      "system": true,
                                      "values": [
                                          "chart"
                                      ],
                                      "type": "entity"
                                  },
                                  {
                                      "system": true,
                                      "values": [
                                          "org"
                                      ],
                                      "type": "visibility"
                                  },
                                  {
                                      "system": true,
                                      "values": [
                                          "busyPercentage",
                                          "cpuTotal"
                                      ],
                                      "type": "metric"
                                  }
                              ],
                              "startTime": -21600,
                              "id": "0306d285-1430-4ff1-87a0-6aa19f6c7b02",
                              "series": [
                                  {
                                      "db": "RAW",
                                      "interval": 0,
                                      "color": "FF0000",
                                      "metrics": [
                                          "md01.iad1.bitscaler.net#cpu#cpuTotal#busyPercentage"
                                      ],
                                      "type": "metric"
                                  },
                                  {
                                      "db": "RAW",
                                      "interval": 0,
                                      "color": "0000FF",
                                      "metrics": [
                                          "md02.iad1.bitscaler.net#cpu#cpuTotal#busyPercentage"
                                      ],
                                      "type": "metric"
                                  }
                              ],
                              "filename": "1402249323355",
                              "ninetyFifths": false,
                              "type": "chart",
                              "endTime": 0
                          }
                      ]
            }


### Update chart [PUT]
Update a chart document.  Only parameters you wish to modify need be included.


+ Request

    + Headers

           Content-Type: application/json
           Authorization: Basic <base64 encoded company:api key>

    + Body
           {
               "startTime": -42600
           }

+ Response 200

   + Headers

           Content-Type: application/json

   + Body
           {
              "message": "Ok",
              "status": 200,
              "totalRecords": 0,
              "records": []
           }


### Delete chart [DELETE]
Delete a chart.

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }


# Group Chart Data API
API methods for retrieving chart data and images

## chart data methods [/api/2.0/chart/data/{id}]

### retrieve charts [GET]
Return chart data by id.  

Parameters
- outputFormat (string) Possible values are png, jpg, igraph, csv, json
- startTime
- endTime
- height (default 600) only applies to image formats
- width (default 800) only applies to image formats
- ninetyFifths (default to false)
- title
- xtitle
- ytitle


+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>

+ Response 200

    Response will be variable depending on what outputFormat is chosen

### create and retrieve chart data [POST]
Create a transient chart.  ID is invalid on creation.  Use this call when you want to fetch a chart but don't have any need to save it.  Refer to the [create chart] documentation for the full list of chart creation options.  Only additional parameters are listed below.

Parameters
- outputFormat (string) Possible values are png, jpg, igraph, csv, json


+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>

    + Body

        + Body

              {
                    "startTime": -21600,
                    "endTime": 0,
                    "filename": "runscope-chart",
                    "series": [
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu0#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu1#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu2#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu3#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu4#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu5#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu6#busyPercentage"
                            ]
                        },
                        {
                            "type": "metric",
                            "db": "RAW",
                            "interval": 0,
                            "metrics": [
                                "ds01.fll1.bitscaler.net#cpu#cpu7#busyPercentage"
                            ]
                        }
                    ]
                }


+ Response 200

    Response will be variable depending on what outputFormat is chosen

# Group Dashboard API
API methods to manipulate dashboard objects.

+ Dashboard Attributes
  + updateInterval (integer optional default 60)  The interval in seconds at which the graphs in the dashboard should be updated
  + filename (string rquried) The name by which the dashboard appears in saved items
  + documents (array of chart UUIDs required) The array of chart or other documents that appear in the chart.  Insert nulls to span columns
  + columns (integer required)  The number of columns to be used in the grid
  + title (string optional) The title that appears on the dashboard
  + timeZone (string optional) Set to override the user or api key timezone
  + tags
  + createTime (timestamp read-only) The epoch timestamp of when this object was created
  + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2

## dashboard methods [/api/2.0/dashboard/{id}]

### create dashboard [POST]
Create a new dashboard.  ID will be autogenerated and returned upon creation.

+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>

    + Body

          {
              "title": "dashboard-title",
              "columns": 1,
              "documents": [
                  {
                      "id": "0a403e71-6c37-4f39-ba89-95f0a5c67335",
                      "doctype": "chart"
                  },
                  {
                      "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                      "doctype": "chart"
                  },
                  {
                      "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                      "doctype": "chart"
                  }
              ],
              "updateInterval": "60",
              "filename": "test-dashboard-filename"
          }


+ Response 200
    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "ca77bdeb-5400-4078-b758-0b0c7b892739",
                      "tags": [
                          {
                              "system": true,
                              "values": [
                                  "bitscaler-API"
                              ],
                              "type": "owner"
                          },
                          {
                              "system": true,
                              "values": [
                                  "dashboard"
                              ],
                              "type": "entity"
                          },
                          {
                              "system": true,
                              "values": [
                                  "org"
                              ],
                              "type": "visibility"
                          }
                      ],
                      "title": "dashboard-title",
                      "documents": [
                          {
                              "id": "0a403e71-6c37-4f39-ba89-95f0a5c67335",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          }
                      ],
                      "columns": 1,
                      "filename": "test-dashboard-filename",
                      "updateInterval": 60
                  }
              ]
          }

### get dashboard [GET]
Return a dashboard object given its ID or return a list of dashboards if no ID is provided

+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>


+ Response 200
    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "ca77bdeb-5400-4078-b758-0b0c7b892739",
                      "tags": [
                          {
                              "system": true,
                              "values": [
                                  "bitscaler-API"
                              ],
                              "type": "owner"
                          },
                          {
                              "system": true,
                              "values": [
                                  "dashboard"
                              ],
                              "type": "entity"
                          },
                          {
                              "system": true,
                              "values": [
                                  "org"
                              ],
                              "type": "visibility"
                          }
                      ],
                      "title": "dashboard-title",
                      "documents": [
                          {
                              "id": "0a403e71-6c37-4f39-ba89-95f0a5c67335",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          }
                      ],
                      "columns": 1,
                      "filename": "test-dashboard-filename",
                      "updateInterval": 60
                  }
              ]
          }

### update dashboard [PUT]
Update a chart document. Only parameters you wish to modify need be included.

+ Request

    + Headers

          Content-Type: application/json
          Authorization: Basic <base64 encoded company:api key>

    + Body

          {
              "updateInterval": 10
          }


+ Response 200
    + Headers

          Content-Type: application/json
          X-Response-Time: 4ms

    + Body

          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "id": "ca77bdeb-5400-4078-b758-0b0c7b892739",
                      "tags": [
                          {
                              "system": true,
                              "values": [
                                  "bitscaler-API"
                              ],
                              "type": "owner"
                          },
                          {
                              "system": true,
                              "values": [
                                  "dashboard"
                              ],
                              "type": "entity"
                          },
                          {
                              "system": true,
                              "values": [
                                  "org"
                              ],
                              "type": "visibility"
                          }
                      ],
                      "title": "runscope-dashboard-title",
                      "documents": [
                          {
                              "id": "0a403e71-6c37-4f39-ba89-95f0a5c67335",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          },
                          {
                              "id": "0cf99f40-4edb-4dee-a74c-8e4e644f1830",
                              "doctype": "chart"
                          }
                      ],
                      "columns": 1,
                      "filename": "runscope-dashboard-filename",
                      "updateInterval": 10
                  }
              ]
          }

### Delete dashboard [DELETE]
Delete a dashboard.

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }

# Group API Keys
API methods to manipulate API keys

+ API Key Attributes
    + description (string optional) A text string describing the purpose of the api key
    + admin (boolean optional) Whether this key has admin capabilities
    + readOnly (boolean optional default true) Whether or not this key is read-only
    + style (uuid optional default default style ID) The style to attach to this api key.  For use in charts
    + timeZone (string optional default UTC) The timezone to use when pulling charts or data
    + locked (boolean optional default false) Whether this key has been locked or not
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2


## apikey methods [/api/2.0/apikey/{id}]


### create apikey [POST]
Create a new apikey

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "description": "NetOps",
            "timeZone": "America/New_York",
            "style": "00000000-0000-0000-0000-000000000000",
            "locked": "No",
            "readOnly": "No",
            "admin": "Yes"
        }

+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "XXXXX-XXXX-XXXXXX",
                    "readOnly": false,
                    "style": "00000000-0000-0000-0000-000000000000",
                    "description": "NetOps",
                    "admin": true,
                    "timeZone": "America/New_York",
                    "locked": false
                }
            ]
        }


### get apikey [GET]
Retrieve an apikey given an ID or retrieve a list of apikeys if not

+ Request

    + Headers

        Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "XXXXX-XXXX-XXXXXX",
                    "readOnly": false,
                    "style": "126c8f12-6801-47ac-8d7c-71518dbed331",
                    "description": "NetOps",
                    "admin": true,
                    "timeZone": "America/Panama",
                    "locked": false
                }
            ]
        }

### update apikey [PUT]
Update an apikey.  You only need to send the attributes you wish to modify.

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "admin": false
        }

+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "XXXXX-XXXX-XXXXXX",
                    "readOnly": false,
                    "style": "00000000-0000-0000-0000-000000000000",
                    "description": "NetOps",
                    "admin": false,
                    "timeZone": "America/New_York",
                    "locked": false
                }
            ]
        }

### Delete apikey [DELETE]
Delete an apikey.

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }

# Group User API
API methods to manipulate users

+ User Attributes
    + id (string required) Users email address
    + admin (boolean optional default false) Whether this user has admin capabilities
    + firstName (string optional) Users first name
    + lastName (string optional) Users last name
    + phoneNumber (string optional) Phone number for the user
    + timeZone (string optional default UTC) The timezone to use when pulling charts or data
    + locked (boolean optional default false) Whether this user has been locked or not
    + chartStyle (uuid optional default style) The UUID of the style you wish to view charts with
    + dashboardStyle (uuid optional default style) The UUID of the style you wish to view dashboards with
    + printStyle (uuid optional default style) The UUID of the style you wish to print with
    + authenticationMethod (string optional default local) Type of authentication for the user (local | google)
    + password (string optional autogernated) The password for this user
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2


## user methods [/api/2.0/user/{id}]

### create user [POST]
create a user

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>
          Content-Type: application/json

    + Body

          {
              "admin": false,
              "authenticationMethod": "local",
              "chartStyle": "00000000-0000-0000-0000-000000000000",
              "dashboardStyle": "00000000-0000-0000-0000-000000000000",
              "firstName": "run",
              "email": "test@bitscaler.com",
              "lastName": "scope",
              "locked": false,
              "phoneNumber": "555 555 5555",
              "printStyle": "00000000-0000-0000-0000-000000000000",
              "timeZone": "UTC"
          }


+ Response 200

      + Headers

             Content-Type: application/json

      + Body

              {
                  "message": "Ok",
                  "status": 200,
                  "totalRecords": 1,
                  "records": [
                                {
                                      "id": "test@bitscaler.com",
                                      "lastName": "scope",
                                      "phoneNumber": "555 555 5555",
                                      "authenticationMethod": "local",
                                      "admin": false,
                                      "printStyle": "00000000-0000-0000-0000-000000000000",
                                      "dashboardStyle": "00000000-0000-0000-0000-000000000000",
                                      "timeZone": "UTC",
                                      "locked": false,
                                      "chartStyle": "00000000-0000-0000-0000-000000000000",
                                      "firstName": "run",
                                      "password": "PasswordParameter that has 1 value(s)"
                                }
                  ]
              }

### get user [GET]
Retrieve a user given an ID or retrieve a list of users if not

+ Request

    + Headers

        Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                {
                    "id": "XXXXX-XXXX-XXXXXX",
                    "readOnly": false,
                    "style": "126c8f12-6801-47ac-8d7c-71518dbed331",
                    "description": "NetOps",
                    "admin": true,
                    "timeZone": "America/Panama",
                    "locked": false
                }
            ]
        }


### update user [PUT]
Update a user.  You only need to send the attributes you wish to modify.

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "admin": false
        }

+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 0,
            "records": []
        }

### Delete user [DELETE]
Delete a user

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }


# Group Style API
API methods to manipulate styles

+ User Attributes
    + name (string required) Thhe human readable name for this style.
    + RenderingType (string optional default POLYLINE) POLYLINE, AREA, STAIR, STAIR_AREA, STACKED_POLYLINE, STACKED_AREA
    + LegendLocation (string optional default BOTTOM) Where to place the legend on the graph.  BOTTOM, RIGHT, LEFT, TOP
    + LegendVisible (boolean optional default true) Whether or not to display the legend.  true or false
    + TerseLegend (boolean optional default false) Attempts to shorten the legend items by de-duping data.
    + FontFamily (string optional default Verdana) Font to use for the style.  Options vary by system.
    + FontSize (integer optional default 10) Font size.
    + FontWeight (string optional default plain) Weight to use for the font family.  plain, bold or italic.
    + StrokeWidth (float optional default 2.0) The width of lines in the charts.  0.1 - 4.0 (2.0 recommended)
    + MarkerType (string optional default NONE) Marker to use for data points on charts.  NONE, CROSS, CIRCLE, DIAMOND, PLUS, SQUARE, TRIANGLE
    + MarkerSize (integer optional default 5) The size of the marker
    + MarkerColor (string optional default FFFFFF) The hex code of the color to be used on markers
    + Background (string optional default FFFFFF) The hex code of the color to be used as the legend background color.
    + BackgroundPaint (string optional default FFFFFF) The hex code of the color to be used as the background color around the chart area (title, xy axis area).
    + PlotAreaBackground (string optional default FFFFFF) The hex code of the color to be used as the background color for the plot area.
    + Foreground (string optional default 000000) The hex code of the color to be used as color for legend text, x/y axis and labels and title.
    + xGridMajorPaint (string optional default 000000) The hex code of the color to be used for the vertical grid lines (set equal to PlotAreaBackground for none visible)
    + yGridMajorPaint (string optional default 000000) The hex code of the color to be used for the horizontal grid lines (set equal to PlotAreaBackground for none visible)
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2


## style methods [/api/2.0/style/{id}]

### create style [POST]
create a style

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>
          Content-Type: application/json

    + Body

          {
              "name": "runscope-style",
              "RenderingType": "AREA",
              "LegendLocation": "BOTTOM",
              "LegendVisible": "1",
              "TerseLegend": "0",
              "FontFamily": "DejaVu LGC Sans Mono",
              "FontWeight": "plain",
              "FontSize": "10",
              "StrokeWidth": "2.0",
              "MarkerType": "NONE",
              "MarkerSize": "5",
              "MarkerColor": "000000",
              "Background": "FFFFFF",
              "BackgroundPaint": "FFFFFF",
              "PlotAreaBackground": "FFFFFF",
              "Foreground": "000000",
              "xGridMajorPaint": "000000",
              "yGridMajorPaint": "000000"
          }


+ Response 200

      + Headers

             Content-Type: application/json

      + Body

              {
                  "message": "Ok",
                  "status": 200,
                  "totalRecords": 1,
                  "records": [
                              {
                                  "TerseLegend": false,
                                  "RenderingType": "AREA",
                                  "Foreground": "000000",
                                  "LegendLocation": "BOTTOM",
                                  "Background": "FFFFFF",
                                  "FontWeight": "plain",
                                  "FontSize": 10,
                                  "xGridMajorPaint": "000000",
                                  "MarkerSize": 5,
                                  "MarkerType": "NONE",
                                  "id": "a33f243e-af3a-4e81-af9c-be8aebe1d099",
                                  "StrokeWidth": 2,
                                  "LegendVisible": true,
                                  "FontFamily": "DejaVu LGC Sans Mono",
                                  "PlotAreaBackground": "FFFFFF",
                                  "MarkerColor": "000000",
                                  "yGridMajorPaint": "000000",
                                  "name": "runscope-style",
                                  "BackgroundPaint": "FFFFFF"
                              }
                            ]
              }

### get style [GET]
Retrieve a style given an ID or retrieve a list of styles if not

+ Request

    + Headers

        Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                        {
                            "TerseLegend": false,
                            "RenderingType": "AREA",
                            "Foreground": "000000",
                            "LegendLocation": "BOTTOM",
                            "Background": "FFFFFF",
                            "FontWeight": "plain",
                            "FontSize": 10,
                            "xGridMajorPaint": "000000",
                            "MarkerSize": 5,
                            "MarkerType": "NONE",
                            "id": "a33f243e-af3a-4e81-af9c-be8aebe1d099",
                            "StrokeWidth": 2,
                            "LegendVisible": true,
                            "FontFamily": "DejaVu LGC Sans Mono",
                            "PlotAreaBackground": "FFFFFF",
                            "MarkerColor": "000000",
                            "yGridMajorPaint": "000000",
                            "name": "runscope-style",
                            "BackgroundPaint": "FFFFFF"
                        }
                      ]
        }


### update style [PUT]
Update a style.  You only need to send the attributes you wish to modify.

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "RenderingType": "POLYLINE"
        }

+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 1,
            "records": [
                        {
                            "TerseLegend": false,
                            "RenderingType": "POLYLINE",
                            "Foreground": "000000",
                            "LegendLocation": "BOTTOM",
                            "Background": "FFFFFF",
                            "FontWeight": "plain",
                            "FontSize": 10,
                            "xGridMajorPaint": "000000",
                            "MarkerSize": 5,
                            "MarkerType": "NONE",
                            "id": "a33f243e-af3a-4e81-af9c-be8aebe1d099",
                            "StrokeWidth": 2,
                            "LegendVisible": true,
                            "FontFamily": "DejaVu LGC Sans Mono",
                            "PlotAreaBackground": "FFFFFF",
                            "MarkerColor": "000000",
                            "yGridMajorPaint": "000000",
                            "name": "runscope-style",
                            "BackgroundPaint": "FFFFFF"
                        }
                      ]
        }

### Delete style [DELETE]
Delete a style

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }

# Group Company API
API methods to manipulate companies.  Not every organization is allowed to have sub companies so these methods will not work in that case.  

+ Company Attributes
    + name (string required) The human readable name for this company.
    + description (string required) A description for the company.
    + mpsAmount (float optional) Cost of what a user pays for a metric/second. (BitScaler Admin Key only)
    + mps (float optional) Number of metrics/sec the company is sending through the system. (BitScaler Admin Key only)
    + isParent (boolean optional default false) Set to true ONLY when creating a Master Company.  (BitScaler Admin Key only)
    + childrenAllowed (boolean optional default false) If this company can set sub companies.  Normally this is false. (BitScaler Admin Key only)
    + children (array not required RESPONSE ONLY) If company has sub companies, they will be populated in this array.
    + createTime (timestamp read-only) The epoch timestamp of when this object was created
    + outputField (optional on getting list Only) Set to the field names you wish to be returned to you on get.  Use multiple for multiple fields IE outputField=field1&outputField=field2

## company methods [/api/2.0/company/{id}]

### create company [POST]
create a company

+ Request

    + Headers

          Authorization: Basic <base64 encoded company:api key>
          Content-Type: application/json

    + Body

          {
              "name": "runscope",
              "description": "runscope-test",
              "isParent": false
          }


+ Response 200

      + Headers

             Content-Type: application/json

      + Body

          {
                "message": "Ok",
                "status": 200,
                "totalRecords": 1,
                "records": [
                              {
                                  "id": "d5979c93-fadd-491c-a1c9-f1ffe3ecbe0c",
                                  "mpsAmount": "11.50",
                                  "parentCompany": "bitscaler",
                                  "mps": 0,
                                  "description": "runscope-test",
                                  "name": "bitscaler:runscope",
                                  "deleted": false,
                                  "childrenAllowed": false
                              }
                            ]
            }  

### get company [GET]
Retrieve a company given an ID or retrieve a list of companies if not

+ Request

    + Headers

        Authorization: Basic <base64 encoded company:api key>


+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
            "message": "Ok",
            "status": 200,
            "totalRecords": 17,
            "records": [
                            {
                                "id": "ee641315-b3e0-447b-b82a-9924ecaca0cc",
                                "mpsAmount": "11.50",
                                "mps": 0,
                                "description": "Bitscaler Technologies Inc.",
                                "name": "bitscaler",
                                "children": [],
                                "deleted": false,
                                "childrenAllowed": true
                            },
                            {
                                "id": "de968fee-650d-4335-8047-c17baaf94ccc",
                                "mpsAmount": "0.00",
                                "parentCompany": "bitscaler",
                                "mps": 54.87,
                                "description": "Test of new style terse stuff",
                                "name": "bitscaler:blah",
                                "deleted": false,
                                "childrenAllowed": false
                            }
                        ]
        }


### update company [PUT]
Update a company.  You only need to send the attributes you wish to modify.

+ Request

    + Headers

        Content-Type: application/json
        Authorization: Basic <base64 encoded company:api key>

    + Body

        {
            "description": "runscope-change"
        }

+ Response 200

    + Headers

        Content-Type: application/json

    + Body

        {
        "message": "Ok",
        "status": 200,
        "totalRecords": 1,
        "records": [
                      {
                          "id": "d5979c93-fadd-491c-a1c9-f1ffe3ecbe0c",
                          "mpsAmount": "11.50",
                          "parentCompany": "bitscaler",
                          "mps": 0,
                          "description": "runscope-change",
                          "name": "bitscaler:runscope",
                          "deleted": false,
                          "childrenAllowed": false
                      }
                  ]
        }

### Delete company [DELETE]
Delete a company

+ Request

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
             "message": "Ok",
             "status": 200,
             "totalRecords": 0,
             "records": []
          }

## list all companies [/api/2.0/company/listAllCompanies]

### get all companies a super admin has access to [GET]
Used by Super Admin's to get a complete list of companies they can perform operations on

+ Request 

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 3,
              "records": [
                  {
                      "id": "a93ade1a-30a1-4f5b-af66-178e1e16f7b5",
                      "mpsAmount": "0.00",
                      "mps": 0,
                      "description": "QA Master Company (Does have sub companies)",
                      "name": "masterCompany",
                      "children": [],
                      "deleted": false,
                      "childrenAllowed": true
                  },
                  {
                      "id": "d489c1f8-2678-4561-8d7c-cfba9d123760",
                      "mpsAmount": "0.00",
                      "parentCompany": "masterCompany",
                      "mps": 0,
                      "description": "QA Sub Company (Does have master company)",
                      "name": "masterCompany:subCompany",
                      "deleted": false,
                      "childrenAllowed": false
                  },
                  {
                      "id": "49ba86e3-44b2-4a53-952c-5632a3ea6c8f",
                      "mpsAmount": "0.00",
                      "mps": 0,
                      "description": "QA Master Company (Does not have sub companies)",
                      "name": "masterNoSubCompany",
                      "children": [],
                      "deleted": false,
                      "childrenAllowed": true
                  }
              ]
          }

## list all subCompanies [/api/2.0/company/listSubCompanies]
### get super admin's subcompanies [GET]
Used by Super Admin's to get a complete list of sub companies they can perform operations on

+ Request 

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 2,
              "records": [
                  {
                      "id": "a93ade1a-30a1-4f5b-af66-178e1e16f7b5",
                      "mpsAmount": "0.00",
                      "mps": 0,
                      "description": "QA Master Company (Does have sub companies)",
                      "name": "masterCompany",
                      "children": [],
                      "deleted": false,
                      "childrenAllowed": true
                  },
                  {
                      "id": "d489c1f8-2678-4561-8d7c-cfba9d123760",
                      "mpsAmount": "0.00",
                      "parentCompany": "masterCompany",
                      "mps": 0,
                      "description": "QA Sub Company (Does have master company)",
                      "name": "masterCompany:subCompany",
                      "deleted": false,
                      "childrenAllowed": false
                  }
              ]
          }

# Group Session API
API methods to view session information.

## get session information [/api/2.0/session/data]
### get session information [GET]
Get the current users session data

+ Request 

    + Headers

           Authorization: Basic <base64 encoded company:api key>

+ Response 200

   + Headers

          Content-Type: application/json

   + Body
          {
              "message": "Ok",
              "status": 200,
              "totalRecords": 1,
              "records": [
                  {
                      "lastName": "Last",
                      "authenticationMethod": "local",
                      "timeZone": "America/New_York",
                      "cid": "a93ade1a-30a1-4f5b-af66-178e1e16f7b5",
                      "id": "user@example.com",
                      "superAdmin": true,
                      "portalAdmin": false,
                      "phoneNumber": "+1 (954) 555-1212",
                      "admin": true,
                      "company": "masterCompany",
                      "dashboardStyle": "00000000-0000-0000-0000-000000000000",
                      "printStyle": "00000000-0000-0000-0000-000000000000",
                      "chartStyle": "00000000-0000-0000-0000-000000000000",
                      "locked": false,
                      "firstName": "First"
                  }
              ]
          }
